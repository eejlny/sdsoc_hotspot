/*
 * *************************************************************************************************************
 *
 * *** SDSoC_lab_design
 *
 * Contains main() and 3 functions
 *    rgb2gray()   - color to gray scale -
 *    sharpen()    - sharpens image -
 *    edgeDetect() - edge detection - uses Sobel edge detection (filter) *tentative*
 *
 * *** definitions
 *    pixels - RGB value - 8 bits per color for a total of 24 bits
 *    window - (kernel) - small region around current pixel, typically 3x3
 *    frame  - full image
 *
 * ***
 *
 * History:
 *    V0.2 - 06/12/2015 - sc, wk         - performance improvements through coding style, cleanup for release
 *    V0.1 - 05/27/2015 - sp, sm, sk, wk - initial version
 *
 * *************************************************************************************************************
 *
 */

/* includes */
#include <stdio.h>
#include <stdlib.h>
#include <sds_lib.h>
#include <sys/time.h>
#include "lab_design.h"
#include "kernelHotspot.h"



/* ambient temperature, assuming no package at all	*/
const float amb_temp_cpu = 80.0;

#define STR_SIZE	256

/* globals */
unsigned long long sw_sds_counter_total[4]     = { 0, 0, 0, 0 };
unsigned int       sw_sds_counter_num_calls[4] = { 0, 0, 0, 0 };
unsigned long long sw_sds_counter[4]           = { 0, 0, 0, 0 };

/* timing related macros */
#define sw_sds_clk_start(IDX) { sw_sds_counter[(IDX)] = sds_clock_counter(); sw_sds_counter_num_calls[(IDX)]++; }
#define sw_sds_clk_stop(IDX) { unsigned long long tmp = sds_clock_counter(); sw_sds_counter_total[(IDX)] += ((tmp < sw_sds_counter[(IDX)]) ? (sw_sds_counter[(IDX)] - tmp): (tmp - sw_sds_counter[(IDX)])); }
#define sw_avg_cpu_cycles(IDX) (sw_sds_counter_total[(IDX)] / sw_sds_counter_num_calls[(IDX)])
#define RGB2GRAY		0
#define SHARPEN			1
#define EDGE_DETECT		2
#define WHOLE_PROCESS   3
#define LOOPS			5
#define TIME_EDGE_DETECT 1


typedef float FLOAT;


//#define OPEN
/* maximum power density possible (say 300W for a 10mm x 10mm chip)	*/
#define MAX_PD	(3.0e6)
/* required precision in degrees	*/
#define PRECISION	0.001
#define SPEC_HEAT_SI 1.75e6
#define K_SI 100
/* capacitance fitting factor	*/
#define FACTOR_CHIP	0.5
//#define OPEN
/* chip parameters	*/
const FLOAT t_chip = 0.0005;
const FLOAT chip_height = 0.016;
const FLOAT chip_width = 0.016;

/* function prototypes */
void dummyfill(float* f);



void check_result(FLOAT *result,  FLOAT *result_fpga, int grid_rows, int grid_cols,int begin,int end,int num_iterations)

{

    int i,j;

    begin++; // bypass the first row full of zeros
    end++;

    for (i=begin; i <= end; i++)
        for (j=0; j < grid_cols; j++)
        {

            if (result[i*grid_cols+j] != result_fpga[i*grid_cols+j])
            {
            	printf("Expected %f and got %f\n",result[i*grid_cols+j],result_fpga[i*grid_cols+j]);
            	printf("computation error at %d\n",i*grid_cols+j);
            	exit(1);
            }
        }
		printf("FPGA and CPU results match !\n");
}


// Returns the current system time in microseconds
long long get_time()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (tv.tv_sec * 1000000) + tv.tv_usec;


}


void fatal(char *s)
{
	fprintf(stderr, "error: %s\n", s);
	exit(1);
}

void read_input(FLOAT *vect, int grid_rows, int grid_cols, char *file)
{
  	int i, j, index;
	FILE *fp;
	char str[STR_SIZE];
	FLOAT val;

	fp = fopen (file, "r");
	if (!fp)
		fatal ("file could not be opened for reading");

	for (i=0; i < grid_rows; i++)
	{
		for(j=0; j< grid_cols; j++)
		{
			  if(i==0 || i==(grid_rows-1) || j==0 || j==(grid_cols-1))
			  {
				  vect[i*grid_cols+j] = 0;
			  }
			  else
			  {
				  fgets(str, STR_SIZE, fp);
			   if (feof(fp))
				  fatal("not enough lines in file");
			   if ((sscanf(str, "%f", &val) != 1) )
				  fatal("invalid file format");
			   vect[i*grid_cols+j] = val;
			  }
			  //printf("loaded data at %d\n",(i+j*grid_cols));
		}
	}

	fclose(fp);
}

void usage(int argc, char **argv)
{
	fprintf(stderr, "Usage: %s  <num_iterations> <temp_file> <power_file>\n", argv[0]);
	fprintf(stderr, "\t<num_iterations>   - number of iterations\n");
	fprintf(stderr, "\t<temp_file>  - name of the file containing the initial temperature values of each cell\n");
	fprintf(stderr, "\t<power_file> - name of the file containing the dissipated power values of each cell\n");
	fprintf(stderr, "\t<begin> - first line to do in fpga\n");
	fprintf(stderr, "\t<end> - last line to do in fpga\n");
	exit(1);
}

void cpu_filter(FLOAT *result, FLOAT *temp, FLOAT *power,
					  FLOAT Cap_1, FLOAT Rx_1, FLOAT Ry_1, FLOAT Rz_1,
					  FLOAT step)
{

    int r, c;
    int row = (FRAME_HEIGHT_ALLOC+2);
    int col = (FRAME_WIDTH_ALLOC+2);



    int r_start = 0;
    int c_start = 0;


    for ( r = 0 ; r < row ; r++ ) {

         for ( c = 0 ; c < col ; c++ ) {

        	 if ((r == 0) || (r == (row-1)) || (c == 0) || (c == (row-1)))
        	 {
					result[r*col + c] = 0;
			 }
			 else
			 {
				/* Update Temperatures */
					result[r*col + c] = temp[r*col + c] +
						(Cap_1 * (power[r*col + c] +
						(temp[(r + 1)*col + c] + temp[(r - 1)*col + c] - 2.f*temp[r*col + c]) * Ry_1 +
						(temp[r*col + c + 1] + temp[r*col + c - 1] - 2.f*temp[r*col + c]) * Rx_1 +
						(amb_temp_cpu - temp[r*col + c]) * Rz_1));
			 }
				//printf("result cpu %f\n", result[r*col + c]);
         }
    }

}



/*
 * *** main()
 *
 * configures data structures, run frame processing
 *
 */
int main(int argc, char* argv[]) {
	// local variables
	int i,j,z;
	int sim_time;
	int begin;
	int end;
	//int input_chunk_size;
	char *tfile, *pfile, *ofile;

	// set up memory structures for moving frames of data
    //float *array_temp = (float*) sds_alloc_non_cacheable(FRAME_HEIGHT_ALLOC * FRAME_WIDTH_ALLOC * sizeof(float));	// color frames are 24 bits per pixel, 32 bits is the smallest un-packed type we can fit it into
	//float *array_power = (float*) sds_alloc_non_cacheable(FRAME_HEIGHT_ALLOC * FRAME_WIDTH_ALLOC * sizeof(float));
	//float *array_out = (float*) sds_alloc_non_cacheable(FRAME_HEIGHT_ALLOC * FRAME_WIDTH_ALLOC * sizeof(float));
	//float *array_out = (float*) sds_alloc_non_cacheable((FRAME_HEIGHT_ALLOC-2) * FRAME_WIDTH_ALLOC * sizeof(float));
    //float *array_temp = (float*) sds_alloc((FRAME_HEIGHT_ALLOC+2) * (FRAME_WIDTH_ALLOC+2) * sizeof(float));	// color frames are 24 bits per pixel, 32 bits is the smallest un-packed type we can fit it into
	//float *array_power = (float*) sds_alloc((FRAME_HEIGHT_ALLOC+2) * (FRAME_WIDTH_ALLOC+2) * sizeof(float));
	//float *array_out = (float*) sds_alloc((FRAME_HEIGHT_ALLOC+2) * (FRAME_WIDTH_ALLOC+2) * sizeof(float));
	float *array_temp = (float*) calloc((FRAME_HEIGHT_ALLOC+2) * (FRAME_WIDTH_ALLOC+2), sizeof(float));	// color frames are 24 bits per pixel, 32 bits is the smallest un-packed type we can fit it into
	float *array_power = (float*) calloc((FRAME_HEIGHT_ALLOC+2) * (FRAME_WIDTH_ALLOC+2), sizeof(float));
	float *array_out = (float*) calloc((FRAME_HEIGHT_ALLOC+2) * (FRAME_WIDTH_ALLOC+2), sizeof(float));
	float *array_temp_cpu = (float*) calloc((FRAME_HEIGHT_ALLOC+2) * (FRAME_WIDTH_ALLOC+2), sizeof(float));	// color frames are 24 bits per pixel, 32 bits is the smallest un-packed type we can fit it into
	//float *array_power_cpu = (float*) calloc(FRAME_HEIGHT * FRAME_WIDTH, sizeof(float));
	float *array_out_cpu = (float*) calloc((FRAME_HEIGHT_ALLOC+2) * (FRAME_WIDTH_ALLOC+2), sizeof(float));

	/* check validity of inputs	*/
		if (argc != 6)
			usage(argc, argv);
		if ((sim_time = atoi(argv[1])) <= 0)
			usage(argc, argv);





	/* read initial temperatures and input power	*/
	tfile = argv[2];
	pfile = argv[3];
	begin = atoi(argv[4]);
	end = atoi(argv[5]);

	printf("Dimensions are %d and %d\n",FRAME_WIDTH_ALLOC,FRAME_HEIGHT_ALLOC);

	printf("Reading data...\n");

	read_input(array_temp, (FRAME_HEIGHT_ALLOC+2), (FRAME_WIDTH_ALLOC+2) , tfile);
	read_input(array_power, (FRAME_HEIGHT_ALLOC+2), (FRAME_WIDTH_ALLOC+2), pfile);
	read_input(array_out, (FRAME_HEIGHT_ALLOC+2), (FRAME_WIDTH_ALLOC+2) , tfile);

	read_input(array_temp_cpu, (FRAME_HEIGHT_ALLOC+2), (FRAME_WIDTH_ALLOC+2) , tfile);
	//read_input(array_power_cpu, FRAME_HEIGHT, FRAME_WIDTH, pfile);
	read_input(array_out_cpu, (FRAME_HEIGHT_ALLOC+2), (FRAME_WIDTH_ALLOC+2) , tfile);

	// let the user know what is happening...
	printf("Done reading...\n");

	// initialize the color frame buffer (initial input into the processesing sequence
	//dummyfill(array_c);
	//dummyfill(array_c_power);



	float grid_height = chip_height / FRAME_HEIGHT_ALLOC;
	float grid_width = chip_width / FRAME_WIDTH_ALLOC;

	float Cap = FACTOR_CHIP * SPEC_HEAT_SI * t_chip * grid_width * grid_height;
	float Rx = grid_width / (2.0 * K_SI * t_chip * grid_height);
	float Ry = grid_height / (2.0 * K_SI * t_chip * grid_width);
	float Rz = t_chip / (K_SI * grid_height * grid_width);

	float max_slope = MAX_PD / (FACTOR_CHIP * t_chip * SPEC_HEAT_SI);
    float step = PRECISION / max_slope / 1000.0;

     float Rx_1=1.f/Rx;
    float Ry_1=1.f/Ry;
    float Rz_1=1.f/Rz;
    float Cap_1 = step/Cap;
    //int chunk_size = CHUNK_SIZE;

	/* process image */


    printf("Doing kernel...\n");


    long long start_time = get_time();

    for (i = sim_time; i !=0; i--)
    {

           cpu_filter(array_out_cpu, array_temp_cpu, array_power, Cap_1, Rx_1, Ry_1, Rz_1, step);

           FLOAT* tmp = array_temp_cpu;
           array_temp_cpu = array_out_cpu;
           array_out_cpu = tmp;
    }

    long long end_time = get_time();

    printf("Ending simulation\n");
    printf("Total time CPU: %.3f seconds\n", ((float) (end_time - start_time)) / (1000*1000));



    long long start_time_fpga = get_time();

	sw_sds_clk_start(EDGE_DETECT);


    int k;

	for (i = sim_time; i != 0; i--) {

		k = kernelHotspot(array_temp,array_power,array_out,Cap_1,Rx_1,Ry_1,Rz_1,begin,end);
		FLOAT* tmp_array = array_temp;
		array_temp = array_out;
		array_out = tmp_array;


	}
	sw_sds_clk_stop(EDGE_DETECT);
	long long end_time_fpga = get_time();

	printf("Done kernel...\n");


	// display time

	printf("Average SW cycles for edge_detect: %llu\n",sw_avg_cpu_cycles(EDGE_DETECT));
	printf("Estimate time: %f seconds\n",((double)sw_avg_cpu_cycles(EDGE_DETECT))/(666.6*1000000));
	printf("Total time FPGA: %.3f seconds\n", ((float) (end_time_fpga - start_time_fpga)) / (1000*1000));



    check_result(array_temp_cpu,array_temp,(FRAME_HEIGHT_ALLOC+2),(FRAME_WIDTH_ALLOC+2),begin,end,sim_time);


	return 0;
}


/*** supporting functions ***/
// fills a color frame with a constant color
void dummyfill(float* f) {
	int i, j;
	for (i = 0; i < FRAME_HEIGHT_ALLOC; i++) {						// loop through all rows
		for (j = 0; j < FRAME_WIDTH_ALLOC; j++) {						// and columns
			int index = (i * FRAME_WIDTH_ALLOC + j);					// compute the offset into the array based on row and column
			f[index] = 324.33;		// and shift each color into its proper position
		}
	}
}
