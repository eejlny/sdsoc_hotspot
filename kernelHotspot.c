#include <stdio.h>
#include <stdlib.h>
#include <sds_lib.h>
#include "edge_detect.h"
#include "lab_design.h"


int kernelHotspot(
float *input_temp,
float *input_power,
float *output,
float Cap_1,
float Rx_1,
float Ry_1,
float Rz_1,
int begin,
int end) {

  int k;
  int line_count = end-begin+1;
  //float *array_temp_fpga = input_temp + begin*FRAME_WIDTH - FRAME_WIDTH;
  //float *array_power_fpga = input_power + begin*FRAME_WIDTH - FRAME_WIDTH;
  //float *array_out_fpga = output + begin*FRAME_WIDTH;
  float *array_temp_fpga = input_temp + begin*FRAME_WIDTH;
  float *array_power_fpga = input_power + begin*FRAME_WIDTH;
  float *array_out_fpga = output + begin*FRAME_WIDTH+FRAME_WIDTH;
  sobel_filter(array_temp_fpga, array_power_fpga, array_out_fpga, Cap_1, Rx_1, Ry_1, Rz_1, line_count);
}
